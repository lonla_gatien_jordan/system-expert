package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * Login Controller.
 */
public class LoginController extends AnchorPane implements Initializable {

    @FXML
    ImageView appLogo;
    @FXML
    TextField username;
    @FXML
    PasswordField password;
    @FXML
    Button login;
    @FXML
    Label errorMessage;

    @FXML
    private Hyperlink register, forgotPassword;

    private SE application;

    public void setApp(SE application) {
        this.application = application;
        this.application.stage.setOnCloseRequest((event) -> {
            try {
                this.application.gotoHome();
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMessage.setText("");
    }

    public void processLogin(ActionEvent event) throws SQLException, IOException {
        if (application == null) {
            errorMessage.setText("Hello " + username.getText());
        } else {
            if (!application.userLogging(username.getText(), password.getText())) {
                errorMessage.setText("Nom d'utilisateur ou mot de passe incorrect");
            }
        }
    }

    public void processRegister(ActionEvent event) {
        if (application == null) {
            return;
        }
        application.userRegister();
    }

    public void processForgotPassword(ActionEvent event) {
        if (application == null) {
            return;
        }

        application.gotoResetPassword();
    }

    public void resetMessage() {
        errorMessage.setText("");
    }
}
