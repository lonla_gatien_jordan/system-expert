package controllers;

import com.jfoenix.controls.JFXDecorator;
import java.io.IOException;
import models.User;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import models.BreakDown;
import models.Component;
import models.Rule;
import models.Symptom;

/**
 * FXML-based Login screen sample
 */
public class SE extends Application {

    public AnchorPane root = new AnchorPane();
    public User loggedUser;
    public Component selectedComponent;
    public Symptom selectedSymptom;
    public BreakDown selectedBreakDown;
    public Rule selectedRule;
    public Stage stage = new Stage();

    @Override
    public void start(Stage primaryStage) throws Exception {
        gotoHome();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public Component getSelectedComponent() {
        return selectedComponent;
    }

    public Symptom getSelectedSymptom() {
        return selectedSymptom;
    }

    public BreakDown getSelectedBreakDown() {
        return selectedBreakDown;
    }

    public Rule getSelectedRule() {
        return selectedRule;
    }

    public boolean userLogging(String username, String password) throws SQLException, IOException {
        if ((new User(username, password)).authenticate()) {
            loggedUser = User.getByUsername(username);
            gotoFactBase();
            return true;
        } else {
            return false;
        }
    }

    void userLogin() {
        loggedUser = null;
        gotoLogin();
    }

    void userRegister() {
        loggedUser = null;
        gotoRegister();
    }

    public void gotoRegister() {
        try {
            RegisterController register
                    = (RegisterController) replaceSceneContent("/views/layouts/Register.fxml",500,500, "Créer un compte",false,false,false);
            register.setApp(this,0);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoAnalyse() {
        try {
            AnalyseController analyse
                    = (AnalyseController) replaceSceneContent("/views/layouts/Analyse.fxml",770,590, "Diagnostic de l'ordinateur",false,true,true);
            analyse.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoResetPassword() {
        try {
            RegisterController register
                    = (RegisterController) replaceSceneContent("/views/layouts/Register.fxml",500,500, "Changer de mot de passe",false,false,false);
            register.setApp(this,1);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoFactBase() {
        try {
            FactBaseController factbase
                    = (FactBaseController) replaceSceneContent("/views/layouts/FactBase.fxml",1000,700, "Mise à jour des bases",false,true,true);
            factbase.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoLogin() {
        try {
            LoginController login
                    = (LoginController) replaceSceneContent("/views/layouts/Login.fxml",500,500, "Connexion",false,false,false);
            login.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoSaveRule() {
        try {
            RuleController rule
                    = (RuleController) replaceSceneContent("/views/layouts/SaveRule.fxml",500,500, "",true,true,true);
            rule.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void gotoSaveBreakDown() {
        try {
            BreakDownController breakDown
                    = (BreakDownController) replaceSceneContent("/views/layouts/SaveBreakDown.fxml",500,500, "",true,true,true);
            breakDown.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoHome() throws IOException {
        try {
            HomeController home
                    = (HomeController) replaceSceneContent("/views/layouts/Home.fxml",770,590, "Computer Diagnostic Utility",false,true,true);
            home.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Initializable replaceSceneContent(String fxml, int height, int width, String title, boolean fullscreen, boolean max, boolean min) throws Exception {
        stage.close();
        FXMLLoader loader = new FXMLLoader();
        InputStream in = SE.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(SE.class.getResource(fxml));
        try {
            stage = new Stage();
            root = (AnchorPane) loader.load(in);
            JFXDecorator decorator = new JFXDecorator(stage, root, fullscreen, max, min);
            decorator.setTitle(title);
            decorator.setCustomMaximize(true);
            Scene scene = new Scene(decorator, height, width);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        } finally {
            in.close();
        }
        return (Initializable) loader.getController();
    }
}
