package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author LONLA GATIEN JORDAN
 */
public class Component {
    public static final boolean EXTERNAL = false;
    public static final boolean INTERNAL = true;
    public CheckBox box = new CheckBox();
    public int id;
    public String name;
    public boolean type = Component.EXTERNAL;
    public ArrayList<BreakDown> SusceptiveBreakDowns = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public CheckBox getBox() {
        return box;
    }
    
    public StringProperty nameProperty(){
        return new SimpleStringProperty(name);
    }
    
    public StringProperty typeProperty(){
        return new SimpleStringProperty(type?"Interne":"Externe");
    }
    
    public ObjectProperty boxProperty(){
        return new SimpleObjectProperty(box);
    }

    public ArrayList<BreakDown> getSusceptiveBreakDowns() throws SQLException {
        String SQL="SELECT * FROM breakdown_has_component WHERE component_id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        ResultSet myResultSet=myStatement.executeQuery();
        SusceptiveBreakDowns.clear();
        if (myResultSet.next()) {
            SusceptiveBreakDowns.add(BreakDown.getById(myResultSet.getInt("breakdown_id")));
        }
        
        return SusceptiveBreakDowns;
    }

    public void setSusceptiveBreakDowns(ArrayList<BreakDown> SusceptiveBreakDowns) {
        this.SusceptiveBreakDowns = SusceptiveBreakDowns;
    }

    public Component(String name, boolean type) {
        this.name = name;
        this.type = type;
    }
    
    public Component(int id, String name, boolean type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
    
    
    
    public boolean save() {
        String SQL="INSERT INTO component (name,type) VALUES(?,?);";
        PreparedStatement myStatement = null;

        try {
            myStatement = Driver.connexion().prepareStatement(SQL);
            myStatement.setString(1, this.name);
            myStatement.setBoolean(2, this.type);
            if (myStatement.executeUpdate() != 0) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    public boolean delete() throws SQLException {
        String SQL="DELETE FROM component WHERE id = ?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        if (myStatement.executeUpdate() != 0) {
            return true;
        }
        return false;
    }

    public static Component getByName(String name) throws SQLException {
        String SQL="SELECT * FROM component WHERE name=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, name);
        ResultSet myResultSet=myStatement.executeQuery();
        if (myResultSet.next()) {
            return new Component(myResultSet.getInt("id"),
                    myResultSet.getString("name"),myResultSet.getBoolean("type"));
        }
        return null;
    }
    
    public static Component getById(int id) throws SQLException {
        String SQL="SELECT * FROM component WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, id);
        ResultSet myResultSet=myStatement.executeQuery();
        if (myResultSet.next()) {
            return new Component(id,
                    myResultSet.getString("name"),myResultSet.getBoolean("type"));
        }
        return null;
    }
    
    public static ArrayList<Component> getListOfComponents() throws SQLException {
        ArrayList<Component> components = new ArrayList<>();
        String SQL = "SELECT * FROM component";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        ResultSet myResultSet = myStatement.executeQuery();
        while (myResultSet.next()) {
            Component component = new Component(myResultSet.getInt("id"), myResultSet.getString("name"), myResultSet.getBoolean("type"));
            component.SusceptiveBreakDowns = component.getSusceptiveBreakDowns();
            components.add(component);
        }
        return components;
    }

    public boolean update(String newName, boolean newType) throws SQLException {
        String SQL="UPDATE component SET name=?,type=? WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, newName);
        myStatement.setBoolean(2, newType);
        myStatement.setInt(3, this.id);
        if (myStatement.executeUpdate() != 0) {
            this.name = newName;
            this.type = newType;
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Component other = (Component) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
}
