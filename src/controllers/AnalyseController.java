package controllers;

import com.jfoenix.controls.JFXButton;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import models.Symptom;

/**
 * FXML Controller class
 *
 * @author LONLA GATIEN JORDAN
 */
public class AnalyseController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    private SE application;
    
    @FXML
    private TextArea question;
    
    @FXML
    private ToggleGroup response;

    @FXML
    private ComboBox<String> list;

    @FXML
    private RadioButton yes;

    @FXML
    private RadioButton no;

    @FXML
    private JFXButton cancel;

    @FXML
    private JFXButton Start;
    
    @FXML
    private ImageView startIcon;
    
    @FXML
    private ProgressIndicator saveResponse;
    
    public int begin = 0, questionNumber = 0;
    
    public ArrayList<Symptom> physical = new ArrayList<>(), beep = new ArrayList<>(), errorcode = new ArrayList<>(), responses = new ArrayList<>();

    @FXML
    void processCancel(ActionEvent event) {
        questionNumber--;
        if(questionNumber < physical.size()){
            if(responses.contains(physical.get(questionNumber))){
                responses.remove(responses.size()-1);
            }
            question.setText(physical.get(questionNumber).getDesignation()+" ?");
            if(questionNumber == physical.size()-1)list.setVisible(false);
        }else if(questionNumber == physical.size()){
            if(responses.get(responses.size()-1).getType() == 2){
                responses.remove(responses.size()-1);
            }
            if(!beep.isEmpty()){
                    question.setText("Y'a-t-il un bip sonore au démarrage ? Si oui, selectionnez le code correspondant dans la liste ci-contre");
                    list.setVisible(true);
                    list.getItems().clear();
                    for(int k = 0; k < beep.size(); k++){
                        list.getItems().add(beep.get(k).getDesignation());
                    }
                    list.getSelectionModel().select(0);
            }
        }
        if(questionNumber == 0)cancel.setDisable(true);
    }

    @FXML
    void processNo(ActionEvent event) throws InterruptedException {
        if(physical == null) physical = new ArrayList();
        if(questionNumber < physical.size()+2){
            animateSave();
        }
    }

    @FXML
    void processStart(ActionEvent event) {
        if(begin == 0){
            begin++;
            Start.setText("Arrêter");
            startIcon.setImage(new Image("/views/ressources/img/stop.png"));
            startAnalysis();
        }else{
            
        }
    }

    @FXML
    void processYes(ActionEvent event) {
        if(physical == null) physical = new ArrayList();
        if(questionNumber < physical.size()+2){
            animateSave();
        }
    }
    
    void startAnalysis(){
        if(!physical.isEmpty()){
            question.setText(physical.get(0).getDesignation()+" ?");
        }
        
    }
 
    public void setApp(SE application){
        this.application = application;
        this.application.stage.setOnCloseRequest((event) -> {
            try {
                this.application.gotoHome();
            } catch (IOException ex) {
                Logger.getLogger(AnalyseController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            cancel.setDisable(true);
            list.setVisible(false);
            saveResponse.setOpacity(0);
            question.setEditable(false);
            physical = Symptom.getListOfSymptoms(1);
            beep = Symptom.getListOfSymptoms(2);
            errorcode = Symptom.getListOfSymptoms(3);
            PrintWriter ecrivain;

    ecrivain =  new PrintWriter(new BufferedWriter
	   (new FileWriter("src//models//base.pl")));
    for(int i = 0; i<errorcode.size(); i++){
        ecrivain.println("ecranBleu("+errorcode.get(i).getDesignation()+")");
        for(int a = 0; a<errorcode.get(i).getListOfRules().size();a++){
            ecrivain.println("ecranBleu("+errorcode.get(i).getDesignation()+",'"+errorcode.get(i).listOfRules.get(a).breakdown.getName()+"'");
        } 
    }
    
  /*  for(int j = 0; j<beep.size(); j++){
        
    }
    
    for(int k = 0; k<physical.size(); k++){
        
    }
   /* ecrivain.println("bonjour, comment cela va-t-il ?");
    ecrivain.println("un peu difficile ?");
    ecrivain.print("On peut mettre des entiers : ");
    ecrivain.println(n);
    ecrivain.print("On peut mettre des instances de Object : ");
    ecrivain.println(new Integer(36));*/
    ecrivain.close();
        } catch (SQLException ex) {
            Logger.getLogger(AnalyseController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AnalyseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    private void animateSave() {
        FadeTransition ft = new FadeTransition(Duration.millis(2000), saveResponse);
        ft.setFromValue(1.0);
        ft.setToValue(0);
        ft.play();
        ft.setOnFinished((event)->{
            if(yes.isSelected()){
                if(questionNumber < physical.size()){
                    responses.add(physical.get(questionNumber));
            }else if(questionNumber == physical.size()){   
               if(!beep.isEmpty()){
                    responses.add(beep.get(list.getSelectionModel().getSelectedIndex()));
               } 
            }else if(questionNumber == physical.size()+1){
               if(!errorcode.isEmpty()){
                  responses.add(errorcode.get(list.getSelectionModel().getSelectedIndex()));  
               }
            }
            }
            questionNumber++;
            if(questionNumber < physical.size()){
                question.setText(physical.get(questionNumber).getDesignation()+" ?");
            }else if(questionNumber == physical.size()){
                if(!beep.isEmpty()){
                    question.setText("Y'a-t-il un bip sonore au démarrage ? Si oui, selectionnez le code correspondant dans la liste ci-contre");
                    list.setVisible(true);
                    list.getItems().clear();
                    for(int k = 0; k < beep.size(); k++){
                        list.getItems().add(beep.get(k).getDesignation());
                    }
                    list.getSelectionModel().select(0);
                }  
            }else if(questionNumber == physical.size()+1){
                if(!errorcode.isEmpty()){
                    question.setText("Y'a-t-il un écran bleu au démarrage ? Si oui, selectionnez le code correspondant dans la liste ci-contre");
                    list.setVisible(true);
                    list.getItems().clear();
                    for(int k = 0; k < errorcode.size(); k++){
                        list.getItems().add(errorcode.get(k).getDesignation());
                    }
                    list.getSelectionModel().select(0);
                }
            }
            if(questionNumber ==  1)cancel.setDisable(false);
            no.setSelected(false);
            yes.setSelected(false);
        });
    }
}
