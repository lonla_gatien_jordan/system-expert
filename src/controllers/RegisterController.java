package controllers;

import java.io.IOException;
import models.User;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * Profile Controller.
 */
public class RegisterController extends AnchorPane implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    
    @FXML
    private Hyperlink login;
    @FXML
    private Button save;

    @FXML
    private Label success, label;

    private SE application;

    public void setApp(SE application, int i) {
        this.application = application;
        this.application.stage.setOnCloseRequest((event) -> {
            try {
                this.application.gotoHome();
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        if(i == 1) label.setText("Nouveau mot de passe :");
        success.setOpacity(0);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    public void goToLogin(ActionEvent event) {
        if (application == null) {
            // We are running in isolated FXML, possibly in Scene Builder.
            // NO-OP.
            return;
        }

        application.userLogin();
    }

    public void saveUser(ActionEvent event) throws SQLException {
        if(label.getText().equals("Nouveau mot de passe :")){
            success.setText("Mot de passe modifié avec success !");
        }else{
            success.setText("Expert enregistré avec success !"); 
        }
        
        if (application == null) {
            // We are running in isolated FXML, possibly in Scene Builder.
            // NO-OP.
            animateMessage();
            return;
        }
        if(username.getText().isEmpty() || username.getText().replaceAll(" ", "").isEmpty() || password.getText().isEmpty() || password.getText().replaceAll(" ", "").isEmpty()){
            success.setText("Certains champs sont vides");
        }else{
            if(label.getText().equals("Nouveau mot de passe :")){
                User user = User.getByUsername(username.getText());
                if(user == null){
                    success.setText("Utilisateur inexistant");
                }else{
                    user.changePassword(password.getText());
                }
            }else{
                if (!(new User(username.getText(), password.getText())).save()) success.setText("Veuillez entrer un autre nom d'utilisateur");
            }
        }
        animateMessage();
    }

    private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), success);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
    
    public void resetMessage(){
        success.setText("");
    }
}
