package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author LONLA GATIEN JORDAN
 */
public class BreakDown {

    public CheckBox box = new CheckBox();
    public int id;
    public String name = "";
    public boolean visibility;
    public ArrayList<Component> AffectedComponents = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public CheckBox getBox() {
        return box;
    }
    
     public StringProperty nameProperty(){
        return new SimpleStringProperty(name);
    }
    
    public ObjectProperty boxProperty(){
        return new SimpleObjectProperty(box);
    }

    public ArrayList<Component> getAffectedComponents() throws SQLException {
        String SQL = "SELECT * FROM breakdown_has_component WHERE breakdown_id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        ResultSet myResultSet = myStatement.executeQuery();
        AffectedComponents.clear();
        while (myResultSet.next()) {
            AffectedComponents.add(Component.getById(myResultSet.getInt("component_id")));
        }
        

        return AffectedComponents;
    }

    public void setAffectedComponents(ArrayList<Component> AffectedComponents) {
        this.AffectedComponents = AffectedComponents;
    }

    public BreakDown() {

    }

    public BreakDown(String name, boolean visibility) {
        this.name = name;
        this.visibility = visibility;
    }

    public BreakDown(int id, String name, boolean visibility) {
        this.id = id;
        this.name = name;
        this.visibility = visibility;
    }

    public boolean save() {
        String SQL = "INSERT INTO breakdown (name,visibility) VALUES(?,?);";
        PreparedStatement myStatement = null;
        try {
            myStatement = Driver.connexion().prepareStatement(SQL);
            myStatement.setString(1, this.name);
            myStatement.setBoolean(2, this.visibility);
            if (!this.AffectedComponents.isEmpty() && myStatement.executeUpdate() != 0) {
                for (int i = 0; i < this.AffectedComponents.size(); i++) {
                    SQL = "INSERT INTO breakdown_has_component(breakdown_id, component_id) VALUES(?,?);";
                    myStatement = Driver.connexion().prepareStatement(SQL);
                    myStatement.setInt(1, BreakDown.getLastId());
                    myStatement.setInt(2, this.AffectedComponents.get(i).getId());
                    myStatement.executeUpdate();
                }
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return false;
    }

    public boolean delete() throws SQLException {
        String SQL = "DELETE FROM breakdown WHERE id = ?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        if (myStatement.executeUpdate() != 0) {
            return true;
        }
        return false;
    }

    public static int getLastId() throws SQLException {
        String SQL = "SELECT MAX(id) FROM breakdown;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        ResultSet myResultSet = myStatement.executeQuery();
        if (myResultSet.next()) {
            return myResultSet.getInt(1);
        }

        return 0;
    }

    public static BreakDown getByName(String name) throws SQLException {
        String SQL = "SELECT * FROM breakdown WHERE name=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, name);
        ResultSet myResultSet = myStatement.executeQuery();
        if (myResultSet.next()) {
            return new BreakDown(myResultSet.getInt("id"),
                    name, myResultSet.getBoolean("visibility"));
        }
        return null;
    }

    public static BreakDown getById(int id) throws SQLException {
        String SQL = "SELECT * FROM breakdown WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, id);
        ResultSet myResultSet = myStatement.executeQuery();
        if (myResultSet.next()) {
            return new BreakDown(id, myResultSet.getString("name"),
                     myResultSet.getBoolean("visibility"));
        }
        return null;
    }
    
    public static ArrayList<BreakDown> getListOfBreakDowns() throws SQLException {
        ArrayList<BreakDown> breakdowns = new ArrayList<>();
        String SQL = "SELECT * FROM breakdown";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        ResultSet myResultSet = myStatement.executeQuery();
        while (myResultSet.next()) {
            BreakDown breakdown = new BreakDown(myResultSet.getInt("id"), myResultSet.getString("name"), myResultSet.getBoolean("visibility"));
            breakdown.AffectedComponents = breakdown.getAffectedComponents();
            breakdowns.add(breakdown);
        }
        
        for(int i = 0; i < breakdowns.size() ; i++){
            final int k = i;
            if(i == 0)breakdowns.get(0).getBox().setSelected(true);
            breakdowns.get(i).getBox().setOnAction((event)->{
                for(int j = 0; j<breakdowns.size(); j++){
                    breakdowns.get(j).getBox().setSelected(k==j);
                }
            }
            );
        }
        return breakdowns;
    }

    public boolean update(String newName, boolean newVisibility, ArrayList<Component> affectedComponents) throws SQLException {
        String SQL = "UPDATE breakdown SET name=?,visibility=? WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, newName);
        myStatement.setBoolean(2, newVisibility);
        myStatement.setInt(3, this.id);
        if (myStatement.executeUpdate() != 0) {
            SQL = "DELETE FROM rule WHERE rule_id = ?;";
            myStatement = Driver.connexion().prepareStatement(SQL);
            myStatement.setInt(1, this.id);
            myStatement.executeUpdate();
            for (int i = 0; i < affectedComponents.size(); i++) {
                SQL = "INSERT INTO breakdown_has_component(breakdown_id, component_id) VALUES(?,?);";
                myStatement = Driver.connexion().prepareStatement(SQL);
                myStatement.setInt(1, this.id);
                myStatement.setInt(2, affectedComponents.get(i).getId());
                myStatement.executeUpdate();
            }

            this.name = newName;
            this.visibility = newVisibility;
            this.AffectedComponents = affectedComponents;
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BreakDown other = (BreakDown) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

}
