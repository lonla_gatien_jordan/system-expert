package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import models.BreakDown;
import models.Component;
import models.Rule;
import models.Symptom;

/**
 * Profile Controller.
 */
public class RuleController extends AnchorPane implements Initializable {

    @FXML
    private TableView<Symptom> table1;

    @FXML
    private TableColumn<Symptom, CheckBox> box1;

    @FXML
    private TableColumn<Symptom, String> symptom;

    @FXML
    private TableView<BreakDown> table2;

    @FXML
    private TableColumn<BreakDown, CheckBox> box2;

    @FXML
    private TableColumn<BreakDown, String> breakdown;

    @FXML
    private Label success;

    @FXML
    private Button save, cancel;

    private SE application;

    @FXML
    void processCancel(ActionEvent event) {

    }

    @FXML
    void saveRule(ActionEvent event) throws SQLException {
        success.setText("Rule successfully created!");
        if (application == null) {
            animateMessage();
            return;
        }
        Rule rule = new Rule();
        rule.symptoms.clear();
        for (int i = 0; i < table1.getItems().size(); i++) {
            if (table1.getItems().get(i).getBox().isSelected()) {
                rule.symptoms.add(table1.getItems().get(i));
            }
        }
        
        for (int i = 0; i < table2.getItems().size(); i++) {
            if (table2.getItems().get(i).getBox().isSelected()) {
                rule.breakdown = table2.getItems().get(i);
                break;
            }
        }
        System.out.println(rule.symptoms.size());
        if (!rule.save()) {
            success.setText("This rule already exists");
        }
    animateMessage();
}

public void setApp(SE application) {
        this.application = application;
        Component selectedComponent = application.getSelectedComponent();
        if (selectedComponent != null) {
            
        }
        success.setOpacity(0);
    }

    @Override
        public void initialize(URL location, ResourceBundle resources) {
        box1.setStyle("-fx-padding: 8 8 8 8;");
        box2.setStyle("-fx-padding: 8 8 8 8;");
        box1.setCellValueFactory(cellData -> cellData.getValue().boxProperty());
        symptom.setCellValueFactory(cellData -> cellData.getValue().designationProperty());
        box2.setCellValueFactory(cellData -> cellData.getValue().boxProperty());
        breakdown.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        try {
            table1.setItems(FXCollections.observableArrayList(Symptom.getListOfSymptoms()));
        

} catch (SQLException ex) {
            Logger.getLogger(RuleController.class
.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            table2.setItems(FXCollections.observableArrayList(BreakDown.getListOfBreakDowns()));
        

} catch (SQLException ex) {
            Logger.getLogger(RuleController.class
.getName()).log(Level.SEVERE, null, ex);
        }
        box1.setSortable(false);
        symptom.setSortable(false);
        box2.setSortable(false);
        breakdown.setSortable(false);
        box1.setMaxWidth(box1.getPrefWidth());
        box1.setMinWidth(box1.getPrefWidth());
        box2.setMaxWidth(box2.getPrefWidth());
        box2.setMinWidth(box2.getPrefWidth());
        table1.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table2.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    @FXML
        public void goToLogin(ActionEvent event) {
        if (application == null) {
            // We are running in isolated FXML, possibly in Scene Builder.
            // NO-OP.
            return;
        }

        application.userLogin();
    }

    private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), success);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
}
