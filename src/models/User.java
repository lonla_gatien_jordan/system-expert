package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author LONLA GATIEN JORDAN
 */
 
public class User {
    private String id;
    private String username = "";
    private String password = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
   
    public User() {
        
    }
    
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public User(String id,String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
 
    
    public boolean save() {
        String SQL="INSERT INTO user (username,password) VALUES(?,?);";
        PreparedStatement myStatement = null;

        try {
            myStatement = Driver.connexion().prepareStatement(SQL);
            myStatement.setString(1, this.username);
            myStatement.setString(2, this.password);
            if (myStatement.executeUpdate() != 0) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    public boolean delete() throws SQLException {
        String SQL="DELETE FROM user WHERE username=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, this.username);
        if (myStatement.executeUpdate() != 0) {
            return true;
        }
        return false;
    }

    public static User getByUsername(String username) throws SQLException {
        String SQL="SELECT * FROM user WHERE username=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, username);
        ResultSet myResultSet=myStatement.executeQuery();
        if (myResultSet.next()) {
            return new User(myResultSet.getString("id"),
                    myResultSet.getString("username"),myResultSet.getString("password"));
        }
        return null;
    }

    public boolean authenticate() throws SQLException {
        String SQL="SELECT * FROM user WHERE username=? and password=?";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, this.username);
        myStatement.setString(2, this.password);
        ResultSet myResultSet=myStatement.executeQuery();
        if (myResultSet.next()) {
            return true;
        }
        return false;
    }

    public boolean changePassword(String newPassword) throws SQLException {
        String SQL="UPDATE user SET password=? WHERE username=? and password=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, newPassword);
        myStatement.setString(2, this.username);
        myStatement.setString(3, this.password);
        if (myStatement.executeUpdate() != 0) {
            this.password = newPassword;
            return true;
        }
        return false;
    }
}
