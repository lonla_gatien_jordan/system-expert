package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author LONLA GATIEN JORDAN
 */
public class Driver {

    /**
     * Connect to a sample database
     */
    public static Connection conn = null;

    public static Connection connexion() {
        // SQLite connection string
        String url = "jdbc:sqlite:src\\models\\sedata.db";
        if (conn == null) {
            try {
                conn = DriverManager.getConnection(url);
                //System.out.println("Connection to SQLite has been established.");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return conn;
    }
}
