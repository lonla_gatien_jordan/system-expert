package controllers;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author LONLA GATIEN JORDAN
 */
public class HomeController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    private SE application;
    
    
    @FXML
    private JFXButton update;

    @FXML
    private JFXButton analyse;

    @FXML
    private Label message;

    @FXML
    private Button details;

    @FXML
    void processAnalyse(ActionEvent event) {
        this.application.gotoAnalyse();
    }

    @FXML
    void processDetails(ActionEvent event) {

    }

    @FXML
    void processUpdate(ActionEvent event) {
        this.application.gotoLogin();
    }
 
    public void setApp(SE application){
        this.application = application;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
