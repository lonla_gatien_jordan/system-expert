package controllers;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.util.Duration;
import models.BreakDown;
import models.Component;
import models.Symptom;

/**
 * FXML Controller class
 *
 * @author LONLA GATIEN JORDAN
 */
public class FactBaseController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    private SE application;

    @FXML
    private TextField componentName;

    @FXML
    private RadioButton isExternal;

    @FXML
    private ToggleGroup type;

    @FXML
    private RadioButton isInternal;

    @FXML
    private Label sucessMessage1, descriptionLabel;

    @FXML
    private JFXButton saveComponent, modifyComponent;

    @FXML
    private TableView<Component> table0;

    @FXML
    private TableColumn<Component, String> col01;

    @FXML
    private TableColumn<Component, String> col02;

    @FXML
    private TextArea designation;

    @FXML
    private ComboBox<String> factType;

    @FXML
    private Label sucessMessage2;

    @FXML
    private JFXButton saveFact;

    @FXML
    private JFXButton modifyFact;

    @FXML
    private TableView<Symptom> table1;

    @FXML
    private TableColumn<Symptom, String> col11;

    @FXML
    private TableColumn<Symptom, String> col12;

    @FXML
    private TextArea description;

    @FXML
    private CheckBox isVisible;

    @FXML
    private Label sucessMessage3;

    @FXML
    private JFXButton savePanne, modifyPanne;

    @FXML
    private TableView<Component> table2;

    @FXML
    private TableColumn<Component, CheckBox> col21;

    @FXML
    private TableColumn<Component, String> col22;

    @FXML
    private TableView<BreakDown> table3;

    @FXML
    private TableColumn<BreakDown, String> col31;

    @FXML
    void processModifyPanne(ActionEvent event) {

    }

    @FXML
    void processSavePanne(ActionEvent event) {
        sucessMessage3.setText("Panne enregistrée avec success !");
        if (application == null) {
            animateMessage3();
            return;
        }
        if(description.getText().isEmpty() || description.getText().replaceAll(" ", "").isEmpty()){
            sucessMessage3.setText("Veuillez décrire la panne");
        }else{
            BreakDown breakDown = new BreakDown(description.getText(),isVisible.isSelected());
            breakDown.AffectedComponents.clear();
            for(int i=0; i< table2.getItems().size(); i++){
                if(table2.getItems().get(i).getBox().isSelected()){
                    breakDown.AffectedComponents.add(table2.getItems().get(i));
                }
            }
            System.out.println(breakDown.AffectedComponents.size());
           if (!breakDown.save()){
               sucessMessage3.setText("Cette panne existe déjà");
           }else{
               modifyPanne.setDisable(true);
               afficheTable2();
               afficheTable3();
           }
        }
        animateMessage3();
    }

    @FXML
    void processFactType(ActionEvent event) {
        if(factType.getSelectionModel().getSelectedIndex() != 0){
            descriptionLabel.setText("Code : ");
        }else{
            descriptionLabel.setText("Description : ");
        }
    }

    @FXML
    void processModifyFact(ActionEvent event) throws SQLException {
        sucessMessage2.setText("Symptôme modifié avec success !");
        if (application == null) {
            animateMessage2();
            return;
        }
        if(designation.getText().isEmpty() || designation.getText().replaceAll(" ", "").isEmpty()){
            sucessMessage2.setText("Le champs description est vide");
        }else{
           if (!this.application.selectedSymptom.update(designation.getText(),factType.getSelectionModel().getSelectedIndex()+1)){
               sucessMessage2.setText("Symptôme existant");
           }else{
               afficheTable1();
               modifyFact.setDisable(true);
               this.application.selectedSymptom = null;
               
           }
        }
        animateMessage2();
    }

    @FXML
    void processSaveFact(ActionEvent event) {
        sucessMessage2.setText("Symptôme enregistré avec success !");
        if (application == null) {
            animateMessage2();
            return;
        }
        if(designation.getText().isEmpty() || designation.getText().replaceAll(" ", "").isEmpty()){
            sucessMessage2.setText("Veuillez donner la description");
        }else{
            System.out.println(factType.getSelectionModel().getSelectedIndex()+1);
           if (!(new Symptom(designation.getText(),factType.getSelectionModel().getSelectedIndex()+1)).save()){
               sucessMessage2.setText("Symptôme existant");
           }else{
               modifyFact.setDisable(true);
               afficheTable1();
           }
        }
        animateMessage2();
    }


    public void setApp(SE application) {
        this.application = application;
        this.application.stage.setOnCloseRequest((event) -> {
            try {
                this.application.gotoHome();
            } catch (IOException ex) {
                Logger.getLogger(FactBaseController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        Component selectedComponent = application.getSelectedComponent();
        sucessMessage1.setOpacity(0);
        sucessMessage2.setOpacity(0);
    }

    private void animateMessage1() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), sucessMessage1);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
    
    public void resetMessage1(){
        sucessMessage1.setText("");
    }
    
    private void animateMessage2() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), sucessMessage2);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
    
    public void resetMessage2() {
        sucessMessage2.setText("");
    }
    
    public void resetMessage3(){
        sucessMessage3.setText("");
    }
    
    private void animateMessage3() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), sucessMessage3);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
    
    @FXML
    void processSaveComponent(ActionEvent event) {
        sucessMessage1.setText("Composant enregistré avec success !");
        if (application == null) {
            animateMessage1();
            return;
        }
        if(componentName.getText().isEmpty() || componentName.getText().replaceAll(" ", "").isEmpty()){
            sucessMessage1.setText("Le champs nom est vide");
        }else{
           if (!(new Component(componentName.getText(),isInternal.isSelected())).save()){
               sucessMessage1.setText("Composant existant");
           }else{
               modifyComponent.setDisable(true);
               afficheTable0();
               afficheTable2();
           }
        }
        animateMessage1();
    }
    
        @FXML
    void processModifyComponent(ActionEvent event) throws SQLException {
        sucessMessage1.setText("Composant modifié avec success !");
        if (application == null) {
            animateMessage1();
            return;
        }
        if(componentName.getText().isEmpty() || componentName.getText().replaceAll(" ", "").isEmpty()){
            sucessMessage1.setText("Le champs nom est vide");
        }else{
           if (!this.application.selectedComponent.update(componentName.getText(),isInternal.isSelected())){
               sucessMessage1.setText("Composant existant");
           }else{
               modifyComponent.setDisable(true);
               this.application.selectedComponent = null;
               afficheTable0();
               afficheTable2();
           }
        }
        animateMessage1();
    }
    
    public void afficheTable0() {
        table0.getItems().clear();
        col01.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        col02.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        try {
            table0.setItems(FXCollections.observableArrayList(Component.getListOfComponents()));
        } catch (SQLException ex) {
            Logger.getLogger(BreakDownController.class.getName()).log(Level.SEVERE, null, ex);
        }
        col01.setSortable(false);
        col02.setSortable(false);
        table0.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table0.setOnContextMenuRequested((event)->{
            this.application.selectedComponent = table0.getSelectionModel().getSelectedItem();
            if(this.application.selectedComponent != null){
                try {
                    this.application.selectedComponent.delete();
                    sucessMessage1.setText("Composant supprimé avec success");
                    componentName.setText("");
                    this.application.selectedComponent = null;
                    afficheTable0();
                } catch (SQLException ex) {
                    Logger.getLogger(FactBaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
                modifyComponent.setDisable(true);
            }
           
        });
        table0.setOnMouseClicked((event)->{
            this.application.selectedComponent = table0.getSelectionModel().getSelectedItem();
            if(this.application.selectedComponent != null){
                modifyComponent.setDisable(false);
            }
            componentName.setText(this.application.selectedComponent.getName());
            isInternal.setSelected(this.application.selectedComponent.getType()); 
            isExternal.setSelected(!this.application.selectedComponent.getType());
            sucessMessage1.setText("");
        });
        table0.setOnMouseExited((event)->{
            table0.getSelectionModel().clearSelection();
            //this.application.selectedComponent = null;
        });
    }
    
    public void afficheTable1() {
        table1.getItems().clear();
        col11.setCellValueFactory(cellData -> cellData.getValue().designationProperty());
        col12.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        try {
            table1.setItems(FXCollections.observableArrayList(Symptom.getListOfSymptoms()));
        } catch (SQLException ex) {
            Logger.getLogger(BreakDownController.class.getName()).log(Level.SEVERE, null, ex);
        }
        col11.setSortable(false);
        col12.setSortable(false);
        table1.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table1.setOnContextMenuRequested((event)->{
            this.application.selectedSymptom = table1.getSelectionModel().getSelectedItem();
            if(this.application.selectedSymptom != null){
                try {
                    this.application.selectedSymptom.delete();
                    sucessMessage2.setText("Symptôme supprimé avec success");
                    designation.setText("");
                    this.application.selectedSymptom = null;
                    afficheTable1();
                } catch (SQLException ex) {
                    Logger.getLogger(FactBaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
                modifyFact.setDisable(true);
            }
           
        });
        table1.setOnMouseClicked((event)->{
            this.application.selectedSymptom = table1.getSelectionModel().getSelectedItem();
            if(this.application.selectedSymptom != null){
                modifyFact.setDisable(false);
            }
            designation.setText(this.application.selectedSymptom.getDesignation());
            factType.getSelectionModel().select(this.application.selectedSymptom.type-1); 
            sucessMessage2.setText("");
        });
        table1.setOnMouseExited((event)->{
            table1.getSelectionModel().clearSelection();
        });
    }
       
public void afficheTable2() {
        table2.getItems().clear();
        col21.setStyle("-fx-padding: 8 8 8 8;");
        col21.setCellValueFactory(cellData -> cellData.getValue().boxProperty());
        col22.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        try {
            table2.setItems(FXCollections.observableArrayList(Component.getListOfComponents()));
        } catch (SQLException ex) {
            Logger.getLogger(BreakDownController.class.getName()).log(Level.SEVERE, null, ex);
        }
        col21.setSortable(false);
        col22.setSortable(false);
        col21.setMaxWidth(col21.getPrefWidth());
        col21.setMinWidth(col21.getPrefWidth());
        table2.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table2.setOnContextMenuRequested((event)->{
           // this.application.selectedSymptom = table1.getSelectionModel().getSelectedItem();
           /* if(this.application.selectedSymptom != null){
                try {
                    this.application.selectedSymptom.delete();
                    sucessMessage2.setText("Symptôme supprimé avec success");
                    designation.setText("");
                    this.application.selectedSymptom = null;
                    afficheTable1();
                } catch (SQLException ex) {
                    Logger.getLogger(FactBaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }*/
            modifyPanne.setDisable(true);
           
        });
        table2.setOnMouseExited((event)->{
            table2.getSelectionModel().clearSelection();
        });
    }
        
public void afficheTable3() {
        table3.getItems().clear();
        col31.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        try {
            table3.setItems(FXCollections.observableArrayList(BreakDown.getListOfBreakDowns()));
        } catch (SQLException ex) {
            Logger.getLogger(BreakDownController.class.getName()).log(Level.SEVERE, null, ex);
        }
        col31.setSortable(false);
        table3.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table3.setOnContextMenuRequested((event)->{
            this.application.selectedBreakDown = table3.getSelectionModel().getSelectedItem();
            if(this.application.selectedBreakDown != null){
                try {
                    this.application.selectedBreakDown.delete();
                    sucessMessage3.setText("Panne supprimée avec success");
                    description.setText("");
                    this.application.selectedBreakDown = null;
                    afficheTable2();
                    afficheTable3();
                } catch (SQLException ex) {
                    Logger.getLogger(FactBaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
                modifyPanne.setDisable(true);
            }
           
        });
        afficheTable2();
        table3.setOnMouseClicked((event)->{
            this.application.selectedBreakDown = table3.getSelectionModel().getSelectedItem();
            if(this.application.selectedBreakDown != null){
                modifyPanne.setDisable(false);
            }
            description.setText(this.application.selectedBreakDown.getName());
            isVisible.setSelected(this.application.selectedBreakDown.isVisibility());
            for(int i = 0; i<table2.getItems().size(); i++){
                table2.getItems().get(i).getBox().setSelected(this.application.selectedBreakDown.AffectedComponents.contains(table2.getItems().get(i)));
            }
            sucessMessage3.setText("");
        });
        table3.setOnMouseExited((event)->{
            table3.getSelectionModel().clearSelection();
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        isExternal.setSelected(true);
        modifyComponent.setDisable(true);
        modifyFact.setDisable(true);
        modifyPanne.setDisable(true);
        factType.getItems().addAll("PHYSIQUE","BEEP SONORE","CODE D'ERREUR");
        factType.getSelectionModel().select(0);
        afficheTable0();
        afficheTable1();
        afficheTable2();
        afficheTable3();
    }    
    
}
