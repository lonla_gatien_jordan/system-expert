package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author LONLA GATIEN JORDAN
 */
public class Rule {

    public int id = 1;
    public BreakDown breakdown = new BreakDown();
    public ArrayList<Symptom> symptoms = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BreakDown getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(BreakDown breakdown) {
        this.breakdown = breakdown;
    }

    public ArrayList<Symptom> getSymptoms() throws SQLException {
        String SQL = "SELECT * FROM rule_has_symptom WHERE rule_id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        ResultSet myResultSet = myStatement.executeQuery();
        symptoms.clear();
        if (myResultSet.next()) {
            symptoms.add(Symptom.getById(myResultSet.getInt("symptom_id")));
        }

        return symptoms;
    }

    public void setSymptoms(ArrayList<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    public Rule() {

    }

    public static int getLastId() throws SQLException {
        String SQL = "SELECT MAX(id) FROM rule;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        ResultSet myResultSet = myStatement.executeQuery();
        if (myResultSet.next()) {
            return myResultSet.getInt(1);
        }

        return 0;
    }

    public Rule(int id, int breakdown_id) throws SQLException {
        this.id = id;
        this.breakdown = BreakDown.getById(breakdown_id);
    }

    public boolean save() throws SQLException {
        String SQL = "INSERT INTO rule (breakdown_id) VALUES(?);";
        PreparedStatement myStatement = null;
        if (!Rule.getListOfRules().contains(this)) {
            try {
                myStatement = Driver.connexion().prepareStatement(SQL);
                myStatement.setInt(1, this.breakdown.getId());
                if (myStatement.executeUpdate() != 0) {
                    for (int i = 0; i < this.symptoms.size(); i++) {
                        SQL = "INSERT INTO rule_has_symptom(rule_id, symptom_id) VALUES(?,?);";
                        myStatement = Driver.connexion().prepareStatement(SQL);
                        myStatement.setInt(1, Rule.getLastId());
                        myStatement.setInt(2, this.symptoms.get(i).getId());
                        myStatement.executeUpdate();
                    }
                    return true;
                }
            } catch (SQLException e) {
                return false;
            }
        }

        return false;
    }

    public boolean delete() throws SQLException {
        String SQL = "DELETE FROM rule WHERE id = ?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        if (myStatement.executeUpdate() != 0) {
            return true;
        }
        return false;
    }

    public static Rule getById(int id) throws SQLException {
        String SQL = "SELECT * FROM rule WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, id);
        ResultSet myResultSet = myStatement.executeQuery();
        if (myResultSet.next()) {
            return new Rule(id, myResultSet.getInt("breakdown_id"));
        }
        return null;
    }

    public static ArrayList<Rule> getListOfRules() throws SQLException {
        ArrayList<Rule> rules = new ArrayList<>();
        String SQL = "SELECT * FROM rule";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        ResultSet myResultSet = myStatement.executeQuery();
        while (myResultSet.next()) {
            Rule rule = new Rule(myResultSet.getInt("id"), myResultSet.getInt("breakdown_id"));
            rule.symptoms = rule.getSymptoms();
            rules.add(rule);
        }
        return rules;
    }

    public boolean update(BreakDown breakdown, ArrayList<Symptom> symptoms) throws SQLException {
        String SQL = "UPDATE rule SET breakdown_id=? WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, breakdown.getId());
        myStatement.setInt(2, this.id);
        if (myStatement.executeUpdate() != 0) {
            SQL = "DELETE FROM rule WHERE rule_id = ?;";
            myStatement = Driver.connexion().prepareStatement(SQL);
            myStatement.setInt(1, this.id);
            myStatement.executeUpdate();
            for (int i = 0; i < symptoms.size(); i++) {
                SQL = "INSERT INTO rule_has_symptom(rule_id, symptom_id) VALUES(?,?);";
                myStatement = Driver.connexion().prepareStatement(SQL);
                myStatement.setInt(1, this.id);
                myStatement.setInt(2, symptoms.get(i).getId());
                myStatement.executeUpdate();
            }
            this.breakdown = BreakDown.getById(breakdown.getId());
            this.symptoms = symptoms;
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rule other = (Rule) obj;
        if (!Objects.equals(this.breakdown, other.breakdown)) {
            return false;
        }
        if (!Objects.equals(this.symptoms, other.symptoms)) {
            return false;
        }

        if (!Objects.equals(this.symptoms, other.symptoms)) {
            return false;
        }
        return true;
    }

}
