package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import models.BreakDown;
import models.Component;

/**
 * Profile Controller.
 */
public class BreakDownController extends AnchorPane implements Initializable {

    @FXML
    private CheckBox isVisible;

    @FXML
    private TextArea name;

    @FXML
    private TableView<Component> table;

    @FXML
    private TableColumn<Component, CheckBox> box;

    @FXML
    private TableColumn<Component, String> component;

    @FXML
    private Label success;

    @FXML
    private Button save,cancel;
    
    private SE application;

    @FXML
    void processCancel(ActionEvent event) {

    }

    @FXML
    void saveBreakDown(ActionEvent event) {
        success.setText("Breakdown successfully created!");
        if (application == null) {
            animateMessage();
            return;
        }
        if(name.getText().isEmpty() || name.getText().replaceAll(" ", "").isEmpty()){
            success.setText("The description field is blank");
        }else{
            BreakDown breakDown = new BreakDown(name.getText(),isVisible.isSelected());
            breakDown.AffectedComponents.clear();
            for(int i=0; i< table.getItems().size(); i++){
                if(table.getItems().get(i).getBox().isSelected()){
                    breakDown.AffectedComponents.add(table.getItems().get(i));
                }
            }
            System.out.println(breakDown.AffectedComponents.size());
           if (!breakDown.save()) success.setText("This description already exists");
        }
        animateMessage();
    }

    public void setApp(SE application) {
        this.application = application;
        Component selectedComponent = application.getSelectedComponent();
        if (selectedComponent != null) {
            name.setText(selectedComponent.getName());
            isVisible.setSelected(selectedComponent.getType());
        }
        success.setOpacity(0);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //isVisible.setSelected(true);
        box.setStyle("-fx-padding: 8 8 8 8;");
        box.setCellValueFactory(cellData -> cellData.getValue().boxProperty());
        component.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        try {
            table.setItems(FXCollections.observableArrayList(Component.getListOfComponents()));
        } catch (SQLException ex) {
            Logger.getLogger(BreakDownController.class.getName()).log(Level.SEVERE, null, ex);
        }
        box.setSortable(false);
        component.setSortable(false);
        box.setMaxWidth(box.getPrefWidth());
        box.setMinWidth(box.getPrefWidth());
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    @FXML
    public void goToLogin(ActionEvent event) {
        if (application == null) {
            // We are running in isolated FXML, possibly in Scene Builder.
            // NO-OP.
            return;
        }

        application.userLogin();
    }

    private void animateMessage() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), success);
        ft.setFromValue(0.0);
        ft.setToValue(1);
        ft.play();
    }
}
