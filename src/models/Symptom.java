package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author LONLA GATIEN JORDAN
 */
public class Symptom {
    public static final int PHYSICAL = 1;
    public static final int BEEP = 2;
    public static final int ERROR_CODE = 3;
    public CheckBox box = new CheckBox();
    public int id;
    public String designation = "";
    public int type = Symptom.PHYSICAL;
    
    public ArrayList<Rule> listOfRules = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public CheckBox getBox() {
        return box;
    }
    
    public StringProperty designationProperty(){
        if(this.type == PHYSICAL){
            return new SimpleStringProperty(designation);
        }else if(this.type == BEEP){
            return new SimpleStringProperty("Beep Sonore : "+designation);
        }else{
            return new SimpleStringProperty("Ecran Bleu : " + designation);
        }
    }
    
    public StringProperty typeProperty(){
        if(this.type == PHYSICAL){
            return new SimpleStringProperty("PHYSIQUE");
        }else if(this.type == BEEP){
            return new SimpleStringProperty("BEEP SONORE");
        }else{
            return new SimpleStringProperty("CODE D'ERREUR");
        }
    }
    
    public ObjectProperty boxProperty(){
        return new SimpleObjectProperty(box);
    }

    public Symptom() {
        
    }

    public Symptom(String designation, int type) {
        this.designation = designation;
        this.type = type;
    }
    
    public Symptom(int id, String designation, int type) {
        this.id = id;
        this.designation = designation;
        this.type = type;
    }
    
    public boolean save() {
        String SQL="INSERT INTO symptom (designation,type) VALUES(?,?);";
        PreparedStatement myStatement = null;

        try {
            myStatement = Driver.connexion().prepareStatement(SQL);
            myStatement.setString(1, this.designation);
            myStatement.setInt(2, this.type);
            if (myStatement.executeUpdate() != 0) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    public boolean delete() throws SQLException {
        String SQL="DELETE FROM symptom WHERE designation=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, this.designation);
        if (myStatement.executeUpdate() != 0) {
            return true;
        }
        return false;
    }

    public static Symptom getByDesignation(String designation) throws SQLException {
        String SQL="SELECT * FROM symptom WHERE designation=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, designation);
        ResultSet myResultSet=myStatement.executeQuery();
        if (myResultSet.next()) {
            return new Symptom(myResultSet.getInt("id"),
                    myResultSet.getString("designation"),myResultSet.getInt("type"));
        }
        return null;
    }
    
    
    public static ArrayList<Symptom> getListOfSymptoms() throws SQLException {
        ArrayList<Symptom> symptoms = new ArrayList<>();
        String SQL = "SELECT * FROM symptom";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        ResultSet myResultSet = myStatement.executeQuery();
        while (myResultSet.next()) {
            symptoms.add(new Symptom(myResultSet.getInt("id"), myResultSet.getString("designation"), myResultSet.getInt("type")));
        }
        return symptoms;
    }
    
    public ArrayList<Rule> getListOfRules() throws SQLException {
        ArrayList<Symptom> symptoms = new ArrayList<>();
        String SQL = "SELECT * FROM rule_has_symptom WHERE symptom_id = ?";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, this.id);
        ResultSet myResultSet = myStatement.executeQuery();
        listOfRules.clear();
        while (myResultSet.next()) {
            listOfRules.add(Rule.getById(myResultSet.getInt("rule_id")));
        }
        return listOfRules;
    }
    
    public static ArrayList<Symptom> getListOfSymptoms(int type) throws SQLException {
        ArrayList<Symptom> symptoms = new ArrayList<>();
        String SQL = "SELECT * FROM symptom WHERE type = ?";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, type);
        ResultSet myResultSet = myStatement.executeQuery();
        while (myResultSet.next()) {
            symptoms.add(new Symptom(myResultSet.getInt("id"), myResultSet.getString("designation"), myResultSet.getInt("type")));
        }
        return symptoms;
    }

    public static Symptom getById(int id) throws SQLException {
        String SQL="SELECT * FROM symptom WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setInt(1, id);
        ResultSet myResultSet=myStatement.executeQuery();
        if (myResultSet.next()) {
            return new Symptom(id,
                    myResultSet.getString("designation"),myResultSet.getInt("type"));
        }
        return null;
    }

    public boolean update(String newDesignation, int newType) throws SQLException {
        String SQL="UPDATE symptom SET designation=?,type=? WHERE id=?;";
        PreparedStatement myStatement = Driver.connexion().prepareStatement(SQL);
        myStatement.setString(1, newDesignation);
        myStatement.setInt(2, newType);
        myStatement.setInt(3, this.id);
        if (myStatement.executeUpdate() != 0) {
            this.designation = newDesignation;
            this.type = newType;
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Symptom other = (Symptom) obj;
        if (!Objects.equals(this.designation, other.designation)) {
            return false;
        }
        return true;
    }
    
    
}
